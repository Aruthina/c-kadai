/*
	TriangleClass.cpp
	TriangleClassクラスの各メンバ関数を定義
*/

//ヘッダーをインクルード
#include<iostream>
#include "TriangleClass.h" //クラスを宣言しているヘッダ

//変数に値を代入する
void TriangleClass::Input()
{
	teihen = 20.0f;
	takasa = 15.0f;
}

//変数同士の計算
void TriangleClass::Calc()
{
	menseki = teihen * takasa / 2.0f;
}

//出力
void TriangleClass::Disp()
{
	std::cout << "三角形の面積=" << menseki << "\n";
}
