#include "Game.h"
#include<iostream>

void Game::GameLoop()
{
	//インスタンス作成
	Player pl;
	Enemy ene;

	//攻撃したときのダメージ量
	int damage;

	for (int turn = 1; ; turn++)
	{
		std::cout << "\n======" << turn << "ターン目======\n";

		//それぞれのHP表示
		pl.DispHp();
		ene.DispHp();

		//Playerの攻撃
		damage = pl.Attack(ene.GetDef());
		ene.Damage(damage);
		if (ene.IsDead())
		{
			break;
		}

		//敵の攻撃
		damage = ene.Attack(pl.GetDef());
		pl.Damage(damage);
		if (pl.IsDead())
		{
			break;
		}
	}
	std::cout << "終了\n";
}