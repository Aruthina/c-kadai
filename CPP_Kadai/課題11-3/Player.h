#pragma once
class Player
{
	//メンバ変数
	int hp; //HP
	int atk; //攻撃力
	int def; //防御力
	//メンバ関数
public:
	Player();
	void DispHp();
	int Attack(int i);
	void Damage(int i);
	int GetDef();
	bool IsDead();
};

