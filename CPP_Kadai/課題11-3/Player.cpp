#include "Player.h"
#include<iostream>
Player::Player()
{
	hp = 300;
	atk = 50;
	def = 35;
}

//HP表示
void Player::DispHp()
{
	std::cout << "プレイヤーのHP=" << hp << "\n";
}

//攻撃
int Player::Attack(int i)
{
	std::cout << "プレイヤーの攻撃!\n";
	return atk - i / 2;
}

//ダメージを受ける
void Player::Damage(int i)
{
	std::cout << "プレイヤーは" << i << "のダメージ\n";
	hp -= i;
}

//防御力取得
int Player::GetDef()
{
	return def;
}

//戦闘不能判定
bool Player::IsDead()
{
	if (hp < 0)
	{
		return true;
	}
	return false;
}