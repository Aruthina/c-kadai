#pragma once
class Fat
{
	char name;
	float height, weight;
public:
	void SetName(char n);
	void SetHeight(float h);
	void SetWeight(float w);
	void Calc();
	float GetBMI();
	void Disp();
};

