#include "Fat.h"
#include <string.h>
//定数宣言

//プロトタイプ宣言
void SetData();
void BmiCalc();
void Sort();
void Output();

//グローバル変数
Fat* human;

int main()
{
	//インスタンスを作成
	human = new Fat;

	SetData();
	BmiCalc();
	Sort();
	Output();

	//インスタンスを削除
	delete human;
}

//全員分の名前、身長、体重を設定
void SetData()
{
	//全員分のデータ(適当に)
	const char *name[] = {"太郎","花子","次郎","良子","吾郎"};
	float height[] = {1.72f,1.63f,1.85f,1.56f,1.77f};
	float weight[] = {68.2f,55.6f,92.5f,63.3f,50.1f};

	//アクセス関数を使って全員分のデータをを渡す

}

//全員分の BMI を計算
void BmiCalc()
{

}

//痩せている順にならべる
void Sort()
{

}

//全員分の結果を表示
void Output()
{

}