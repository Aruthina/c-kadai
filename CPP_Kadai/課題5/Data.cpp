/*
	Data.cpp
	Data クラスの各メンバ関数を定義
*/

//ヘッダーをインクルード
#include "Data.h"
#include<iostream>

//渡された値をメンバ変数に代入する(アクセス関数)
void Data::SetValue(int a)
{
	value = a;
}

//メンバ変数の内容を出力するするメンバ関数
void Data::Disp()
{
	std::cout << "メンバ変数の値は" << value << "\n";
}