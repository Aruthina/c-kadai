#include<iostream>
#include"Data.h"

//メイン関数
int main()
{
	int i;
	//変数iに適当な値を代入
	std::cin >> i;

	//Dataクラスのインスタンス(実態)を作る
	Data x;

	//メンバ関数を呼び出す
	x.SetValue(i); //変数iの値を渡す
	x.Disp(); //表示する
}