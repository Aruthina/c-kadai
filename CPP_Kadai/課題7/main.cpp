#include<iostream>
#include"Status.h"

int main()
{
	//レベル
	int lv; 

	 //クラスのインスタンス
	Status st;

	std::cout << "レベルを入力⇒";
	std::cin >> lv;

	//変数lvの値を渡す(セッター)
	st.SetLv(lv);
	st.Calc();

	//表示
	std::cout << "HP=" << st.GetHp() << "\n";
	std::cout << "攻撃力=" << st.GetAtk() << "\n";
	std::cout << "防御力=" << st.GetDef() << "\n";

}