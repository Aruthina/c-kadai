/*
	Status.cpp
	Statusクラスのメンバ関数を定義
*/

//ヘッダーをインクルード
#include "Status.h"

//レベル値を受け取る
void Status::SetLv(int i)
{
	lv = i;
}
//各パラメータを計算
void Status::Calc()
{
	hp = lv * lv + 50;
	atk = lv * 10;
	def = lv * 9;
}

//HPを取得
int Status::GetHp()
{
	return hp;
}
//攻撃力を取得
int Status::GetAtk()
{
	return atk;
}

//防御力を取得
int Status::GetDef()
{
	return def;
}
