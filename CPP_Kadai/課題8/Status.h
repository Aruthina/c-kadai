#pragma once
/*
	Status.h
	Status クラス宣言
*/

//クラスを宣言
class Status
{
	//メンバ変数
private:
	int lv, hp, atk, def;
	//メンバ関数
public:
	//セッター
	bool SetLv(int i);
	void Calc();
	//ゲッター
	int GetHp();
	//ゲッター
	int GetAtk();
	//ゲッター
	int GetDef();

};

