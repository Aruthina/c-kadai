/*
	Sample.cpp
	Sampleクラスの各メンバ関数を定義
*/

//ヘッダーをインクルード
#include "Sample.h"
#include <iostream>

//変数に値を代入する
void Sample::Input1()
{
	x = 10;
}
//変数を作り値を代入する
void Sample::Input2()
{
	int x;
	x = 20;
}

//変数を出力
void Sample::Disp()
{
	std::cout << x;
}

