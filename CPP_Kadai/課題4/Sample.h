#pragma once
/*
	Sample.h
	Sample クラス宣言
*/

//クラスを宣言
class Sample
{
	//メンバ変数
	int x;

	//メンバ関数
public:
	void Input1();
	void Input2();
	void Disp();
};

