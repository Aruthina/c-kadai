/*
	Calclation.cpp
	Calclation クラスのメンバ関数を定義
*/

//ヘッダーをインクルード
#include "Calclation.h"
#include<iostream>

//渡された値をメンバ変数に代入(セッター)
void Calclation::SetA(float a)
{
	value1 = a;
}
//渡された値をメンバ変数に代入(セッター)
void Calclation::SetB(float b)
{
	value2 = b;
}
//メンバ変数を出力するメンバ関数
void Calclation::Disp()
{
	//足し算
	std::cout << value1 << "+" << value2 << "=" << value1 + value2<<"\n";
	//引き算
	std::cout << value1 << "-" << value2 << "=" << value1 - value2 << "\n";
}
