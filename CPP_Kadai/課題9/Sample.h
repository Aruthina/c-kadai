#pragma once
/*
	Sample.h
	Sample クラス宣言
*/

//クラスを宣言
class Sample
{
	//メンバ関数
public:
	Sample(); //コンストラクタ
	~Sample(); //デストラクタ
	void memberFunc();
};

