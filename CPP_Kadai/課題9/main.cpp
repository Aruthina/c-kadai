#include "Sample.h"
#include<iostream>
void Function();

int main()
{
	std::cout << "プログラム開始\n";
	Function();
	std::cout << "プログラム終了\n";
}

void Function()
{
	std::cout << "関数が呼び出されました\n";

	//インスタンス作成
	Sample instSample;
	//メンバ関数実行
	instSample.memberFunc();
}

////予想////
//プログラム開始
//関数が呼び出されました
//コンストラクタが呼び出されました
//クラスのメンバ関数が呼び出されました
//デストラクタが呼びだされました
//プログラム終了