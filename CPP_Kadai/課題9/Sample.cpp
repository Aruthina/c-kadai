/*
	Sample.cpp
	Sampleクラスのメンバ関数を定義
*/

//ヘッダーをインクルード
#include "Sample.h"
#include<iostream>

//コンスタント
Sample::Sample()
{
	std::cout << "コンストラクタが呼び出されました\n";
}
//デストラクタ
Sample::~Sample()
{
	std::cout << "デストラクタが呼び出されました\n";
}

void Sample::memberFunc()
{
	std::cout << "クラスのメンバ関数が呼び出されました\n";
}