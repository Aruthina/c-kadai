/*
	Sample.cpp
	SampleClass クラスの各メンバ関数を定義
*/

//ヘッダーをインクルード
#include<iostream>
#include"Sample.h" //クラスを宣言しているヘッダ

//変数に値を代入する
void Sample::Input()
{
	a = 10;
	b = 3;
}

//変数同士の計算
void Sample::Plus()
{
	c = a + b;
}

//出力
void Sample::Disp()
{
	std::cout << "変数cの値は" << c << "\n";
}
