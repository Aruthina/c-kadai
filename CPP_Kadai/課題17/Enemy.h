#pragma once
#include "Character.h"
class Enemy : public Character
{
	//メンバ関数
public:
	Enemy();
	void DispHp();
	int Attack(int i);
	void Damage(int i);
};

