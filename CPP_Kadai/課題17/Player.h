#pragma once
#include "Character.h"
class Player : public Character
{

	//メンバ関数
public:
	Player();
	void DispHp();
	int Attack(int i);
	void Damage(int i);

};

