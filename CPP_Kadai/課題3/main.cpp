#include"CircleClass.h"

//メイン関数
int main()
{
	//CircleClassのインスタンス(実態)を作る
	CircleClass a;

	//メンバ関数を呼び出す
	a.Input();
	a.Calc();
	a.Disp();
}