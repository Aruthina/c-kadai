#pragma once
/*
	CircleClass.h
	CircleClass クラス宣言
*/

//クラスを宣言
class CircleClass
{
	//メンバ変数
	float r;
	float area;
	//メンバ関数
public:
	void Input();
	void Calc();
	void Disp();
};

