/*
	CircleClass.cpp
	CircleClass クラスのメンバ関数を定義
*/

//ヘッダーをダウンロード
#include "CircleClass.h"
#include<iostream>

//変数に値代入
void CircleClass::Input()
{
	std::cout << "半径は？";
	std::cin >> r;
}
//面積を求める関数
void CircleClass::Calc()
{
	area = r * r * 3.14f;
}
//出力
void CircleClass::Disp()
{
	std::cout << "円の面積は"<<area<<"\n";
}
