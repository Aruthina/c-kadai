/*
	Calclation.cpp
	Calclation クラスのメンバ関数を定義
*/

//ヘッダーをインクルード
#include<iostream>
#include"Calculation.h"

//渡された値をメンバ変数に代入(セッター)
void Calculation::SetA(float a)
{
	value1 = a;
}
//渡された値をメンバ変数に代入(セッター)
void Calculation::SetB(float b)
{
	value2 = b;
}
//メンバ変数を出力するメンバ関数
void Calculation::Disp()
{
	//足し算
	std::cout << value1 << "+" << value2 << "=" << value1 + value2 << "\n";
	//引き算
	std::cout << value1 << "-" << value2 << "=" << value1 - value2 << "\n";
}
